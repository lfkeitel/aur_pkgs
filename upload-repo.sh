#!/bin/bash
set -e

GPG_KEY='lee@keitel.xyz'
PKGDEST="${1:-$PWD/.repo}"

cd "$PKGDEST"
repo-add -s -k $GPG_KEY ./personal.db.tar.zst ./*.pkg.tar.zst
rm -rf *.old *.old.*
rsync -rl -zz ./ root@arch-repo.keitel.xyz:/srv/repo/

cd ..
scp ./after_html_content.html root@arch-repo.keitel.xyz:/srv/repo/.assets/after_html_content.html

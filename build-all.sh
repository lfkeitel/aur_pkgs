#!/bin/bash
set -e

GPG_KEY='lee@keitel.xyz'

pkgs=(
    calcurse-patched
    st-patched
    unclutter-patched
    nitrogen-lang-git
    yubico-fido-udev
    coreutils-adv
    poe
    event-driven-servers
    lfk-intel-gmmlib
    lfk-intel-media-driver
)

export PKGDEST="$PWD/.repo"
export SRCDEST="$PWD/.sources"
export SRCPKGDEST="$PWD/.sources"
export BUILDDIR="$PWD/.build"

# Clear out old packages
rm -rf ${PKGDEST}/*.pkg.tar.*

if [[ $1 == 'clean' ]]; then
    rm -rf ${PKGDEST}/*
fi

./clean-chroot.sh

# Build everything
./build.sh $@ ${pkgs[@]}

# Clean up build directory
rm -rf "$BUILDDIR"

confirmPromptY() {
    read -r -p "$1 [Y/n]? " response

    if [[ $response =~ ^([nN][oO]|[nN])$ ]]; then
        echo "n"
        return
    fi
    echo "y"
}

# Upload repo to file server
response="$(confirmPromptY "Would you like to push new repo?")"
if [ "$response" = "y" ]; then
    ./upload-repo.sh "$PKGDEST"
fi

#!/bin/bash
set -e

PKGDEST="${1:-$PWD/.repo}"

cd "$PKGDEST"
rsync 'root@arch-repo.keitel.xyz:/srv/repo/*' ./

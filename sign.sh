#!/bin/bash
set -e

GPG_KEY='lee@keitel.xyz'
CHROOT_DIR="${CHROOT_DIR:-$HOME/chroot}"
CHROOT_BUILD="${CHROOT_BUILD:-1}"

export PKGDEST="$PWD/.repo"
export SRCDEST="$PWD/.sources"
export SRCPKGDEST="$PWD/.sources"
export BUILDDIR="$PWD/.build"
export LOGDEST="$PWD/.logs"

for pkg in "$@"; do
    pushd $pkg >/dev/null

    pkgfile="$(makepkg --packagelist)"
    if [ ! -f $pkgfile ]; then
        pkgfile="${pkgfile}.zst"
    fi
    echo "=> Signing package"
    gpg -u "$GPG_KEY" --output "$pkgfile.sig" --detach-sig --sign "$pkgfile"

    popd >/dev/null
done

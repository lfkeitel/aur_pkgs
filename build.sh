#!/bin/bash
set -e

GPG_KEY='lee@keitel.xyz'
CHROOT_DIR="${CHROOT_DIR:-$HOME/chroot}"
CHROOT_BUILD="${CHROOT_BUILD:-y}"

export PKGDEST="$PWD/.repo"
export SRCDEST="$PWD/.sources"
export SRCPKGDEST="$PWD/.sources"
export BUILDDIR="$PWD/.build"
export LOGDEST="$PWD/.logs"
FORCE=''
if [[ $1 == '-f' ]]; then
    FORCE='-f'
    shift
fi

mkdir -p "$PKGDEST"
mkdir -p "$SRCDEST"
mkdir -p "$BUILDDIR"
mkdir -p "$LOGDEST"

cp ./makepkg.conf $BUILDDIR/makepkg.conf

for pkg in "$@"; do
    rm -rf "$PKGDEST/$pkg"*
    pushd $pkg >/dev/null

    if [ "$CHROOT_BUILD" = 'y' ]; then
        sudo makechrootpkg \
            -c \
            -u \
            -r "$CHROOT_DIR" \
            -d "$BUILDDIR:/build" \
            -d "$PKGDEST:/pkgdest" \
            -d "$SRCPKGDEST:/srcpkgdest" \
            -d "$SRCDEST:/srcdest" \
            -d "$LOGDEST:/logdest" \
            -- -Acsr $FORCE --config /build/makepkg.conf

        pkgfile="$(makepkg --packagelist --config ../makepkg.conf)"
        echo "=> Signing package"
        gpg -u "$GPG_KEY" --output "$pkgfile.sig" --detach-sig --sign "$pkgfile"

        NAMCAP_LOG="$LOGDEST/$(basename "$pkgfile")-namcap.log"
        namcap "$pkgfile" > "$NAMCAP_LOG"
        cat "$NAMCAP_LOG"
    else
        makepkg -Acsr --sign --key $GPG_KEY $FORCE
    fi

    popd >/dev/null
done
